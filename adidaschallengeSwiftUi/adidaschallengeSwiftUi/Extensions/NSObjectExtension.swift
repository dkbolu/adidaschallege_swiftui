//
//  NSObjectExtension.swift
//  cbframework
//
//  Created by Doruk Kaan Bolu on 1.11.2021.
//

import UIKit

extension NSObject {
    func po() {
        debugLog("\(self)")
    }
}

private func debugLog(_ message: @autoclosure () -> String) {
//    if ApplicationContext.shared.debugMod {
        print("\(message())")
//    }
}

