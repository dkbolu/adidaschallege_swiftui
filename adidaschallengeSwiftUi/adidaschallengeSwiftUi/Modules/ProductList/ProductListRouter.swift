//
//  ProductListRouter.swift
//  adidaschallengeSwiftUi
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import SwiftUI

class ProductListRouter {
    func toProductDetailView(product: Product) -> some View {
        let presenter = ProductDetailPresenter(interactor: ProductDetailInteractor(product: product))
        return ProductDetailView(presenter: presenter)
    }
}
