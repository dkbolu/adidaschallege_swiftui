//
//  ProductItemView.swift
//  adidaschallengeSwiftUi
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import SwiftUI
import Kingfisher

struct ProductItemView: View {
    @ObservedObject var product: Product

    var body: some View {
        GeometryReader { geometry in
            ZStack {
                    Color.white
                        .edgesIgnoringSafeArea(.all)
            HStack {
                KFImage(URL(string: product.imgUrl)!)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: geometry.size.width * 0.4)
                    .cornerRadius(15)
                VStack(alignment: .leading) {
                    Text(product.name)
                        .frame(maxWidth: .infinity,
                               maxHeight: .infinity,
                               alignment: .leading)
                        .font(.title2)
                    Text(product.description)
                        .frame(maxWidth: .infinity,
                               maxHeight: .infinity,
                               alignment: .leading)
                        .fixedSize(horizontal: false, vertical: true)
                        .font(.body)
                        .lineLimit(4)
                    Text(product.priceFormatted)
                        .frame(maxWidth: .infinity,
                               maxHeight: .infinity,
                               alignment: .leading)
                        .font(.callout)
                }.foregroundColor(.black)
            }
            .frame(height: 120)
            .padding(EdgeInsets(top: 12,
                                leading: 8,
                                bottom: 12,
                                trailing: 8))
            }
        }
        .cornerRadius(20)
        .clipped()
    }
}

struct ProductItemView_Previews: PreviewProvider {
    static var previews: some View {
        let model = Product.sampleProduct
        ProductItemView(product: model)
    }
}
