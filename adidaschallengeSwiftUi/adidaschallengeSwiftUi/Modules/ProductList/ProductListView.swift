//
//  ProductListView.swift
//  adidaschallengeSwiftUi
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import SwiftUI

struct ProductListView: View {
    @ObservedObject var presenter: ProductListPresenter
    @State var searchText: String = ""

    var body: some View {
        ScrollView {
            PullToRefresh(coordinateSpaceName: "pullToRefresh") {
                presenter.getProducts()
            }
            LazyVStack {
                TextField("Search", text: $searchText)
                .padding(EdgeInsets(top: 4,
                                    leading: 8,
                                    bottom: 4,
                                    trailing: 8))
                .background(Color(.systemGray6))
                .cornerRadius(8)
                ForEach(presenter.getProducts(searchText: searchText), id: \.id) {
                    item in
                    presenter.linkBuilder(product: item) {
                        ProductItemView(product: item)
                            .frame(height: 144)
                    }
                }
                Spacer().background(Color("paleGrey"))
            }.padding(EdgeInsets(top: 4,
                                 leading: 8,
                                 bottom: 4,
                                 trailing: 8))
        }
        .coordinateSpace(name: "pullToRefresh")
        .background(Color("paleGrey"))
        .onAppear(perform: {
            presenter.getProducts()
        })
        .navigationBarTitle("Products", displayMode: .automatic)
    }
}

struct ProductListView_Previews: PreviewProvider {
    static var previews: some View {
        let interactor = ProductListInteractor()
        let presenter = ProductListPresenter(interactor: interactor)
        interactor.sampleProducts()
        return ProductListView(presenter: presenter)
    }
}


struct PullToRefresh: View {

    var coordinateSpaceName: String
    var onRefresh: ()->Void

    @State var needRefresh: Bool = false

    var body: some View {
        GeometryReader { geo in
            if (geo.frame(in: .named(coordinateSpaceName)).midY > 50) {
                Spacer()
                    .onAppear {
                        needRefresh = true
                    }
            } else if (geo.frame(in: .named(coordinateSpaceName)).maxY < 10) {
                Spacer()
                    .onAppear {
                        if needRefresh {
                            needRefresh = false
                            onRefresh()
                        }
                    }
            }
            HStack {
                Spacer()
                if needRefresh {
                    ProgressView()
                } else {
                    Text("")
                }
                Spacer()
            }
        }.padding(.top, -50)
    }
}
