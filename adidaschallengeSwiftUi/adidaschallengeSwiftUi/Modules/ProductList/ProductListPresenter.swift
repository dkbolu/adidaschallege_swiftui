//
//  ProductListPresenter.swift
//  adidaschallengeSwiftUi
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import Foundation
import Combine
import SwiftUI

class ProductListPresenter: ObservableObject {
    private let interactor: ProductListInteractor
    private let router = ProductListRouter()

    @Published var products: [Product] = []
    private var cancellables = Set<AnyCancellable>()

    init(interactor: ProductListInteractor) {
        self.interactor = interactor
        self.interactor.$productList
            .assign(to: \.products, on: self)
            .store(in: &cancellables)
    }

    func getProducts() {
        interactor.getProducts()
    }

    func getProducts(searchText: String) -> [Product] {
        return products.filter({ product in
            searchText.isEmpty ||
                product.name.lowercased().contains(searchText.lowercased()) ||
                product.description.lowercased().contains(searchText.lowercased())
        })
    }

    func linkBuilder<Content: View>(product: Product, @ViewBuilder content: () -> Content) -> some View {
        NavigationLink(
            destination: router.toProductDetailView(product: product)) {
            content()
        }.buttonStyle(FlatLinkStyle())
    }

    struct FlatLinkStyle: ButtonStyle {
        func makeBody(configuration: Configuration) -> some View {
            configuration.label
        }
    }
}
