//
//  ProductListInteractor.swift
//  adidaschallengeSwiftUi
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import Foundation

class ProductListInteractor {
    @Published var productList: [Product] = []

    func sampleProducts() {
        self.productList = Array(Product.sampleProductList.prefix(2))
    }

    func getProducts() {
        NetworkManager()
            .request(endpoint: ProductService.getProducts) { (response: [Product]?, error) in
                
            guard let response = response else {
                error?.localizedDescription.po()
                return
            }
            self.productList = response
        }
    }
}

