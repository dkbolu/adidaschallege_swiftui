//
//  ProductDetailInteractor.swift
//  adidaschallengeSwiftUi
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import Foundation

class ProductDetailInteractor {
    @Published var product: Product
    @Published var reviews: [ProductReview] = []

    init(product: Product) {
        self.product = product
    }

    func sampleReviews() {
        reviews = ProductReview.sampleReviewList
    }

    func getReviews() {
        NetworkManager()
            .request(
                endpoint: ProductReviewService.getReviews(productId: product.id)) { (response: [ProductReview]?, error) in
                
                guard let response = response else {
                    error?.localizedDescription.po()
                    return
                }
                self.reviews = response.reversed()
            }
    }
}
