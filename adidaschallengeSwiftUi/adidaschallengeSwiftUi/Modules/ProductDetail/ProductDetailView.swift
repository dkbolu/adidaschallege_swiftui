//
//  ProductDetailView.swift
//  adidaschallengeSwiftUi
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import SwiftUI

struct ProductDetailView: View {
    @ObservedObject var presenter: ProductDetailPresenter
    @State private var showingDetail = false

    var body: some View {
        VStack {
            ProductItemView(product: presenter.product)
                .frame(maxHeight: 200)
            Spacer()
            Text("Reviews")
                .padding(EdgeInsets(top: 0,
                                    leading: 8,
                                    bottom: 0,
                                    trailing: 0))
                .frame(maxWidth: .infinity, alignment: .leading)
            List {
                ForEach (presenter.reviews, id: \.id) { review in
                    ReviewView(review: review)
                }
            }
            HStack {
                Spacer()
                Button(action: {
                    showingDetail = true
                }) {
                    Image(systemName: "text.bubble")
                    Text("Add Review")
                }.frame(maxWidth: .infinity, maxHeight: 44)
                .foregroundColor(.white)
                .background(Color.black)
                .sheet(isPresented: $showingDetail,
                       onDismiss: {
                        presenter.getReviews()
                       },
                       content: presenter.showAddReviewPopUp2())
//                .fullScreenCover(isPresented: $showingDetail,
//                                 content: presenter.showAddReviewPopUp2())
                Spacer()
            }
            Spacer()
            Spacer()
        }.background(Color("paleGrey"))
        .onAppear(perform: {
            presenter.getReviews()
        })
//        .overlay(showingDetail ? presenter.showAddReviewPopUp() : nil)
        .navigationBarTitle(presenter.product.name, displayMode: .automatic)
    }
}

struct ProductDetailView_Previews: PreviewProvider {
    static var previews: some View {
        let intereactor = ProductDetailInteractor(product: Product.sampleProduct)
        let presenter = ProductDetailPresenter(interactor: intereactor)
        intereactor.sampleReviews()
        return ProductDetailView(presenter: presenter)
    }
}
