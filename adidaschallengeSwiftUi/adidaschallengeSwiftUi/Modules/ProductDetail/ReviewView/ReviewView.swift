//
//  ReviewView.swift
//  adidaschallengeSwiftUi
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import SwiftUI

struct ReviewView: View {
    @ObservedObject var review: ProductReview
    var body: some View {
        HStack {
            Spacer()
            Text(review.text)
                .frame(maxWidth: .infinity, alignment: .leading)
                .font(.subheadline)
            Spacer()
            Spacer()
            Image(systemName: "star.fill")
                .foregroundColor(.yellow)
            Text("\(review.rating)")
                .font(.footnote)
            Spacer()
            Spacer()
        }
    }
}

struct ReviewView_Previews: PreviewProvider {
    static var previews: some View {
        let review = ProductReview.sampleReview
        ReviewView(review: review)
    }
}
