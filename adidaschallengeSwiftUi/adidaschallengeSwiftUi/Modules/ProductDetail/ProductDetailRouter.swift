//
//  ProductDetailRouter.swift
//  adidaschallengeSwiftUi
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import Foundation
import SwiftUI

class ProductDetailRouter {
    func toAddReviewView(productId: String) -> some View {
        let interactor = AddReviewInteractor(productId: productId)
        let presenter = AddReviewPresenter(interactor: interactor)
        return AddReviewView(presenter: presenter)
    }
}
