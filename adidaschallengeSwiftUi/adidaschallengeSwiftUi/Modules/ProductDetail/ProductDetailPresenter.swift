//
//  ProductDetailPresenter.swift
//  adidaschallengeSwiftUi
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import Foundation
import Combine
import SwiftUI

class ProductDetailPresenter: ObservableObject {
    private let interactor: ProductDetailInteractor
    private let router = ProductDetailRouter()
    private var cancellables = Set<AnyCancellable>()

    @Published var product = Product()
    @Published var reviews = [ProductReview]()

    init(interactor: ProductDetailInteractor) {
        self.interactor = interactor

        self.interactor.$product
            .assign(to: \.product, on: self)
            .store(in: &cancellables)

        self.interactor.$reviews
            .assign(to: \.reviews, on: self)
            .store(in: &cancellables)
    }

    func getReviews() {
        interactor.getReviews()
    }

    func showAddReviewPopUp() -> some View {
        return self.router.toAddReviewView(productId: self.product.id)
    }

    func showAddReviewPopUp2() -> () -> AddReviewView {
        return {
            self.router.toAddReviewView(productId: self.product.id) as! AddReviewView
        }
    }
}
