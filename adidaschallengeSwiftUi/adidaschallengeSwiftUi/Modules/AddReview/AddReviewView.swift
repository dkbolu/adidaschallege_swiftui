//
//  AddReviewView.swift
//  adidaschallengeSwiftUi
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import SwiftUI

struct AddReviewView: View {
    @ObservedObject var presenter: AddReviewPresenter
    @State private var reviewText = ""
    @State private var rating = 0
    @Environment(\.presentationMode) var presentationMode
    @State private var showingAlert = false

    var body: some View {
        GeometryReader { metrics in
            ZStack {
                Color("paleGrey")
                    .edgesIgnoringSafeArea(.all)
                VStack {
                    Spacer()
                    VStack (alignment: .trailing){
                        HStack {
                            Button(action: {
                                presentationMode.wrappedValue.dismiss()
                            }) {
                                Image(systemName: "multiply").foregroundColor(.black)
                            }
                        }.padding(12)
                        HStack {
                            Spacer()
                            ForEach ((1...5), id: \.self) {i in
                                Button(action: {
                                    rating = i
                                }) {
                                    let imageName = rating >= i ? "star.fill" : "star"
                                    Image(systemName: imageName)
                                        .foregroundColor(.yellow)
                                        .aspectRatio(contentMode: .fill)
                                        .frame(maxWidth: .infinity,
                                               maxHeight: .infinity)
                                }
                                .frame(maxWidth: .infinity,
                                       maxHeight: .infinity)
                            }
                            Spacer()
                        }.frame(height: metrics.size.height * 0.07)
                        .frame(maxWidth: .infinity)
                        HStack {
                            Spacer()
                            TextEditor(text: $reviewText)
                                .foregroundColor(.secondary)
                                .border(Color.black, width: 1)
                            Spacer()
                        }
                        HStack {
                            Spacer()
                            Button(action: {
                                if reviewText.isEmpty || rating == 0 {
                                    showingAlert = true
                                } else {
                                    presenter.postReview(text: reviewText,
                                                         rating: rating)
                                    presentationMode.wrappedValue.dismiss()
                                }
                            }) {
                                Image(systemName: "square.and.pencil")
                                Text("Send Review")
                            }
                            .alert(isPresented: $showingAlert) {
                                        Alert(title: Text("Important message"), message: Text("You must rate and write a comment. "), dismissButton: .default(Text("Ok")))
                                    }
                            .frame(maxWidth: .infinity, maxHeight: 44)
                            .foregroundColor(.white)
                            .background(Color.black)
                            Spacer()
                        }
                        Spacer()
                    }
                    .frame(maxWidth: .infinity)
                    .background(Color.white)
                    Spacer()
                }
                .frame(height: metrics.size.height * 0.6)
                .padding(12)
            }
        }.background(Color.clear)
    }
}

struct AddReviewView_Previews: PreviewProvider {
    static var previews: some View {
        let interactor = AddReviewInteractor(productId: "HI333")
        let presenter = AddReviewPresenter(interactor: interactor)
        AddReviewView(presenter: presenter)
    }
}
