//
//  AddReviewPresenter.swift
//  adidaschallengeSwiftUi
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import Foundation

class AddReviewPresenter: ObservableObject {
    private let interactor: AddReviewInteractor

    init(interactor: AddReviewInteractor) {
        self.interactor = interactor
    }

    func postReview(text: String, rating: Int) {
        interactor.postReview(text: text, rating: rating)
    }
}
