//
//  AddReviewInteractor.swift
//  adidaschallengeSwiftUi
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import Foundation

class AddReviewInteractor {
    private let productId: String

    init(productId: String) {
        self.productId = productId
    }

    func postReview(text: String, rating: Int) {
        let locale = Locale.current.identifier
        let model = ProductReview()
        model.locale = locale
        model.productId = productId
        model.rating = rating
        model.text = text
        NetworkManager()
            .request(endpoint: ProductReviewService.postReview(model: model)) { (response: ProductReview?, error) in

                guard response != nil else {
                    error?.localizedDescription.po()
                    return
                }
            }
    }
}
