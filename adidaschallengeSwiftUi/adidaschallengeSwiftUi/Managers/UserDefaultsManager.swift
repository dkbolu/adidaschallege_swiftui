//
//  UserDefaultsManager.swift
//  cbframework
//
//  Created by Doruk Kaan Bolu on 1.11.2021.
//

import Foundation

class UserDefaultsManager: NSObject {

    static let sharedInstance = UserDefaultsManager()

    // MARK: String
    func writeString(string: String, key: String) {
//        "UserDefaultsManager writeString : string:\(string) key:\(key)".po()
        UserDefaults.standard.set(string, forKey: key)
    }

    func readString(key: String) -> String {
        let string = UserDefaults.standard.string(forKey: key) ?? ""
//        "UserDefaultsManager readString : string:\(string) key:\(key)".po()

        return string
    }

    // MARK: Int
    func writeInteger(integer: NSInteger, key: String) {
        "UserDefaultsManager writeInteger : integer:\(integer) key:\(key)".po()
        UserDefaults.standard.set(integer, forKey: key)
    }

    func readInteger(key: String) -> Int {
        let integer = UserDefaults.standard.integer(forKey: key)
        "UserDefaultsManager readInteger : integer:\(integer) key:\(key)".po()
        return integer
    }

    // MARK: Bool
    func writeBool(bool: Bool, key: String) {
        "UserDefaultsManager writeBool : bool:\(bool) key:\(key)".po()
        UserDefaults.standard.set(bool, forKey: key)
    }

    func readBool(key: String) -> Bool {
        let bool = UserDefaults.standard.bool(forKey: key)
        "UserDefaultsManager readBool : bool:\(bool) key:\(key)".po()
        return bool
    }

    // MARK: - Object
    func writeObject<T: Codable>(object: T, key: String) {
        "UserDefaultsManager writeObject : object:\(object) key:\(key)".po()
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(object) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: key)
        }
    }

    func readObject<T: Codable>(type: T.Type, key: String) -> T? {
        if let object = UserDefaults.standard.object(forKey: key) as? Data {
            let decoder = JSONDecoder()
            do {
                if let returnObject = try decoder.decode(T?.self, from: object) {
                    return returnObject
                }
            } catch {
                error.localizedDescription.po()
            }
        }
        return nil
    }

    // MARK: NSNumber Sequence
    func writeSequence(sequence: [NSNumber], key: String) {
        "UserDefaultsManager writeSequence : sequence:\(sequence) key:\(key)".po()
        UserDefaults.standard.set(sequence, forKey: key)
    }

    func readSequence(key: String) -> [NSNumber] {
        let seq = UserDefaults.standard.array(forKey: key)
        "UserDefaultsManager readSequence : seq:\(seq ?? []) key:\(key)".po()
        return (seq ?? []) as? [NSNumber] ?? []
    }

    // MARK: Remove
    func removeAllKeys() {
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        UserDefaults.standard.synchronize()
    }

    func removeObject(key: String) {
        UserDefaults.standard.removeObject(forKey: key)
    }
}
