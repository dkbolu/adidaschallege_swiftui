//
//  ContentView.swift
//  adidaschallengeSwiftUi
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            ProductListView(presenter: ProductListPresenter(interactor: ProductListInteractor()))
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
