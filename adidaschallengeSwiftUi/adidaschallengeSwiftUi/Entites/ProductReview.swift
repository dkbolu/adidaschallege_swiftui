//
//  ProductReview.swift
//  adidaschallengeSwiftUi
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import Foundation

final public class ProductReview: Codable, ObservableObject {
    var productId = ""
    var locale = ""
    var rating = 0
    var text = ""
    var id: String {
        return UUID().uuidString
    }

    static let sampleReviewList: [ProductReview] = Bundle.main.decode([ProductReview].self, from: "reviews.json")

    static let sampleReview: ProductReview = sampleReviewList[0]
}
