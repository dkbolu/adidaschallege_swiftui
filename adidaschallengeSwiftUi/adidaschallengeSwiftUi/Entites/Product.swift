//
//  Product.swift
//  adidaschallengeSwiftUi
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import Foundation

final class Product: Decodable, ObservableObject {
    var id = ""
    var name = ""
    var description = ""
    var imgUrl = ""
    var currency = ""
    var price = 0.0
    var priceFormatted: String {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2

        return "\(formatter.string(from: NSNumber(value: price)) ?? "0.00") \(currency)"
    }

    static var sampleProductList: [Product] {
        Bundle.main.decode([Product].self, from: "productList.json")
    }

    static var sampleProduct: Product {
        sampleProductList[0]
    }
}
